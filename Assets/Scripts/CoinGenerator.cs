﻿﻿using UnityEngine;
using System.Collections;

public class CoinGenerator : MonoBehaviour
{
    public GameObject coinPrefab;

    public float coinSpawnRate;

    private float leftBorder;
    private float rightBorder;
    private float topBorder;

    private new Camera camera;

    void Start()
    {
        camera = GameObject.FindObjectOfType<Camera>();
        Debug.Log(camera);
        CalculateBorders();
        InvokeRepeating("AllocateGenerators", 0.0000001f, coinSpawnRate);
    }

    void AllocateGenerators()
    {
        GenerateCoin(leftBorder-1, Random.Range(topBorder/2, topBorder), Random.Range(1f, rightBorder), 2.5f);
        GenerateCoin(Random.Range(leftBorder, rightBorder), topBorder+1, Random.Range(-0.3f, 0.3f), 0);
    }

    void GenerateCoin(float coinGenerateX, float coinGenerateY, float coinVelocityX, float coinVelocityY)
    {
        GameObject newCoin = Instantiate(coinPrefab, new Vector3(coinGenerateX, coinGenerateY, 0), Quaternion.identity) as GameObject;
        newCoin.GetComponent<Rigidbody2D>().velocity = new Vector2(coinVelocityX, coinVelocityY);
    }

    void CalculateBorders()
    {
        leftBorder = camera.ScreenToWorldPoint(new Vector3(0, 0, 10)).x;
        rightBorder = camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 10)).x;
        topBorder = camera.ScreenToWorldPoint(new Vector3(0, Screen.height, 10)).y;
    }
}