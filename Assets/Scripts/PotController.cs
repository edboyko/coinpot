﻿﻿using UnityEngine;
using System.Collections;

public class PotController : MonoBehaviour
{
    private float minX, maxX;
    private float screenLeftBorder, screenRightBorder;
    public new Camera camera;

    void Start()
    {
        CalculateBorders();
        minX = screenLeftBorder + transform.localScale.x / 2;
        maxX = screenRightBorder - transform.localScale.x / 2;
    }

    void Update()
    {
        MoveWithMouse();
    }

    void MoveWithMouse()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, 0, 0);
        float controlX = camera.ScreenToWorldPoint(mousePos).x;
        transform.position = new Vector3(Mathf.Clamp(controlX, minX, maxX), transform.position.y, transform.position.z);
    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Coin")
        {
            Destroy(col.gameObject);
        }
    }

    public void CalculateBorders()
    {
        screenLeftBorder = camera.ScreenToWorldPoint(new Vector3(0, 0, 10)).x;
        screenRightBorder = camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 10)).x;
    }
}